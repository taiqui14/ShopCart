<?php
if (isset($_POST['send'])) {
    header('Location: index.php');
    exit;
}
?>

<?=template_header('Contact')?>
<div class="recentlyadded content-wrapper">
<form class="contact" method="post" action="contact.php">
	<input type="email" name="email" placeholder="Your Email Address" required>
	<input type="text" name="name" placeholder="Your Name" required>
	<input type="text" name="subject" placeholder="Subject" required>
	<textarea name="msg" placeholder="What would you like to contact us about?" required></textarea>
	<div class="buttons">
            <input type="submit" value="Send" name="send">
        </div>
</form>
</div>

<?=template_footer()?>